import React from 'react';
import { NavLink } from 'react-router-dom';

export default class extends React.Component<{}>{

    public render() {
        return (
            <React.Fragment>
                <h1>Hello world from App component!!</h1>
                <NavLink to="/test">Тест</NavLink>

            </React.Fragment>
        );
    }
}