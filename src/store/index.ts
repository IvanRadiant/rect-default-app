import { combineReducers, Dispatch, Action, AnyAction } from 'redux';
import { all, fork } from 'redux-saga/effects';


import exampleSaga from './example/sagas'
import { exampleReducer } from './example/reducer';
import { ExampleState } from './example/types';


// The top-level state object
export interface ApplicationState {
    examples: ExampleState,
}

export interface ConnectedReduxProps<A extends Action = AnyAction> {
    dispatch: Dispatch<A>
}

export const rootReducer = combineReducers<ApplicationState>({
    examples: exampleReducer,
})


export function* rootSaga() {
    yield all([fork(exampleSaga)]/*, fork(teamsSaga)]*/);
}