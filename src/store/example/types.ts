export type ApiResponse = Record<string, any>;

export interface Example extends ApiResponse {
    id: number,
    name: string
}


export const enum ExampleActionTypes {
    FETCH_REQUEST = '@@heroes/FETCH_REQUEST',
    FETCH_SUCCESS = '@@heroes/FETCH_SUCCESS',
    FETCH_ERROR = '@@heroes/FETCH_ERROR'
}

export interface ExampleState {
    readonly loading: boolean;
    readonly data: Example[];
    readonly errors?: string;
}