import React from 'react';
import ReactDOM from 'react-dom';
import { createHashHistory, createBrowserHistory } from 'history';

import configureStore from './configureStore';
import Root from './root';


const initialState = window.initialReduxState;

// We use hash history because this example is going to be hosted statically.
// Normally you would use browser history.
const history = createBrowserHistory();
const store = configureStore(history, initialState);

ReactDOM.render(
    <Root store={store} history={history} />,
    document.getElementById("root")
);